<?php

namespace Pterodactyl\Http\Requests\Server\Settings;

use Pterodactyl\Http\Requests\Server\ServerFormRequest;
use Pterodactyl\Models\Server;

class ChangeServerNameRequest extends ServerFormRequest
{
    /**
     * Rules to use when validating the submitted data.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => Server::getCreateRules()['name'],
        ];
    }

    /**
     * Permission to use when checking if a user can access this resource.
     *
     * @return string
     */
    protected function permission(): string
    {
        return 'edit-name';
    }
}
