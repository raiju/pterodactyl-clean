<?php

namespace Pterodactyl\Http\Requests\Server;

use Pterodactyl\Http\Requests\FrontendUserFormRequest;
use Pterodactyl\Models\Server;

abstract class ServerFormRequest extends FrontendUserFormRequest
{
    /**
     * Determine if a user has permission to access this resource.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!parent::authorize()) {
            return false;
        }

        return $this->user()->can($this->permission(), $this->getServer());
    }

    /**
     * Return the user permission to validate this request against.
     *
     * @return string
     */
    abstract protected function permission(): string;

    public function getServer(): Server
    {
        return $this->attributes->get('server');
    }
}
