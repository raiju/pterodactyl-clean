<?php

namespace Pterodactyl\Http\Requests\Api\Application\Servers;

use Pterodactyl\Contracts\Repository\ServerRepositoryInterface;
use Pterodactyl\Exceptions\Repository\RecordNotFoundException;
use Pterodactyl\Http\Requests\Api\Application\ApplicationApiRequest;
use Pterodactyl\Models\Server;
use Pterodactyl\Services\Acl\Api\AdminAcl;

class GetExternalServerRequest extends ApplicationApiRequest
{
    /**
     * @var string
     */
    protected $resource = AdminAcl::RESOURCE_SERVERS;
    /**
     * @var int
     */
    protected $permission = AdminAcl::READ;
    /**
     * @var \Pterodactyl\Models\Server
     */
    private $serverModel;

    /**
     * Determine if the requested external user exists.
     *
     * @return bool
     */
    public function resourceExists(): bool
    {
        $repository = $this->container->make(ServerRepositoryInterface::class);

        try {
            $this->serverModel = $repository->findFirstWhere([
                ['external_id', '=', $this->route()->parameter('external_id')],
            ]);
        } catch (RecordNotFoundException $exception) {
            return false;
        }

        return true;
    }

    /**
     * Return the server model for the requested external server.
     *
     * @return \Pterodactyl\Models\Server
     */
    public function getServerModel(): Server
    {
        return $this->serverModel;
    }
}
