<?php

namespace Pterodactyl\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Contracts\CleansAttributes;
use Sofa\Eloquence\Contracts\Validable as ValidableContract;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Validable;

class Setting extends Model implements CleansAttributes, ValidableContract
{
    use Eloquence, Validable;

    /**
     * @var array
     */
    protected static $applicationRules = [
        'key' => 'required|string|between:1,255',
        'value' => 'string',
    ];
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';
    /**
     * @var array
     */
    protected $fillable = ['key', 'value'];
}
