<?php

namespace Raiju\Http\Controllers\Base;

use Raiju\Http\Controllers\Controller;

/**
 * Created by PhpStorm.
 * User: atlas
 * Date: 3/12/2019
 * Time: 2:30 PM
 */
class IndexController extends controller
{
    /**
     * Debug to test if a controller entry-point works or not
     *
     * @return void
     */
    public function index()
    {
        dd('test');
    }
}