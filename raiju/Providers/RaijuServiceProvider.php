<?php
/**
 * Created by PhpStorm.
 * User: atlas
 * Date: 3/12/2019
 * Time: 8:17 PM
 */

namespace Raiju\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RaijuServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * This namespace is the main entry-point for the Raiju namespace.
     *
     * @var string
     */
    protected $namespace = 'Raiju\Http\Controllers';


    /**
     * Define the routes for the application.
     */
    public function map()
    {
        $this->mapRaijuRoutes();
    }

    /**
     *
     * This is the main entrypoint to introduce new route files into pterodactyl. This is the cleanest way to add without introducing vendor packages.
     * @return void
     */
    protected function mapRaijuRoutes()
    {
        /**
         * Extended admin routes
         */
        Route::middleware(['web', 'auth', 'csrf'])
            ->namespace($this->namespace . '\Base')
            ->group(base_path('routes/raiju/base.php'));
    }
}